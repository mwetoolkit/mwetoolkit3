#! /usr/bin/env python3

from setuptools import setup
from setuptools import find_packages
MWETK_VERSION = '3.1'


setup(
    name='MWEToolkit',
    version=MWETK_VERSION,
    description='The Multiword Expressions toolkit library',
    author_email='mwetoolkit@gmail.com',
    url='http://mwetoolkit.sourceforge.net',
    packages=find_packages(include=["mwetk*"]),
)
