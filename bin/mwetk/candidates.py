from mwetk.base.candidate import CandidateFactory
from mwetk.base.frequency import Frequency
from mwetk.base.ngram import Ngram
from mwetk.filetype import parse_gen, parse_entities
from mwetk.base.sentence import Sentence

import collections

################################################################################


def iter_sentences(file_list):
    """ Generator for all sentences in a file list
    """
    for entity in parse_gen(file_list):
        if type(entity) is Sentence:
            yield entity

################################################################################


def get_patterns(file_list):
    """ Retrieves entities from a pattern files i.e. patterns.
    file_list needs to be a pattern file.
    """
    return parse_entities(file_list)

################################################################################


def extract_candidates_from_sentences(sentences, dataset_name, patterns):
    """ Iterates through sentences, extracting MWE candidates into `all_entities` and 
    finializes/builds candidates classes at `build_candidates`. Assigns `dataset_name` as dataset
    and extractes using `patterns`.
    """
    all_entities = collections.OrderedDict()
    for sentence in sentences:
        add_matches_from_sentence(
            all_entities, patterns, sentence, dataset_name)
    return build_candidates(all_entities)

################################################################################


def extract_candidates_from_file(dataset_name, file_path, patterns):
    """ Extracts MWE candidates from a single file at file_path using loaded patterns and assigning
    dataset_name as dataset.
    """
    return extract_candidates_from_sentences(iter_sentences([file_path]), dataset_name, patterns)

################################################################################


def add_matches_from_sentence(all_entities,
                              input_patterns,
                              sentence,
                              current_corpus_name,
                              match_distance="All",
                              id_order=["*"],
                              non_overlapping=False,
                              ignore_pos=False,
                              surface_instead_lemmas=False):
    """
    Extracts candidates from a sentece.

    @param all_entities OrderedDict keeping extracted candidates.
    Will be updated using matches from this sentece.

    @param input_patterns Patterns used for candidate extraction

    @param sentence Sentence entity to be processed, extracting candidates

    @param current_corpus_name Corpus/Dataset name from which sentence to be processed came from

    @param match_distance 
    Select the distance through which patterns will match (default: "All"):
    * Distance "Shortest": Output the shortest matches (non-greedy).
    * Distance "Longest": Output the longest matches (greedy).
    * Distance "All": Output all match sizes.

    @param id_order Output tokens in the pattern ID order given by the
    colon-separated <list-of-ids>.  The special ID "*" can be used
    to represent "all remaining IDs". Default: "*".

    @param non_overlapping Do not output overlapping word matches.
    This option should not be used if match-distance is "All".

    @param ignore_pos Ignores parts of speech when counting candidate occurences. 
    This means, for example, that "like" as a preposition and "like" as a verb 
    will be counted as the same entity. Default false.


    @param surface Counts surface forms instead of lemmas. Default false.
    """

    # Each candidate needs to be extracted only once, but 2 patterns
    # can extract the same candidate. `already_matched` handles this
    already_matched = set()

    for pattern in input_patterns:
        pattern_matches = pattern.matches(
            sentence,
            match_distance=match_distance,
            id_order=id_order,
            overlapping=not non_overlapping
        )

        for (match_ngram, wordnums) in pattern_matches:
            wordnums_string = ",".join(str(wn+1) for wn in wordnums)

            # Assures that each candidate is only processed once
            if wordnums_string in already_matched:
                continue
            already_matched.add(wordnums_string)

            # Handling parameters
            if ignore_pos:
                match_ngram.foreach_del_prop("pos")

            # Creates basestring before possibly rmving surface
            ngram_basestring_specific = str(match_ngram.to_string())

            if surface_instead_lemmas:
                match_ngram.foreach_del_prop("lemma")
            else:
                for word in match_ngram:
                    # Still uses surface if lemma is unavailable
                    if word.has_prop("lemma"):
                        word.del_prop("surface")

            # TODO create a CandidateSet class to abstract away this grouping
            # of stuff based on general-vs-specific info
            ngram_basestring_general = str(match_ngram.to_string())

            # Returns reference (python) for value with key=ngram_basestring_general
            # If there is no object with this key, a new one is created
            info_for_ngram_basestring = all_entities.setdefault(
                ngram_basestring_general, {})
            (surfaces_dict, total_freq) = info_for_ngram_basestring \
                .get(current_corpus_name, ({}, 0))
            freq_surface = surfaces_dict.setdefault(
                ngram_basestring_specific, [])

            # Append the id of the source sentence. The number of items in
            # surfaces_dict[form] is the number of occurrences of that form.
            source_sent_id = str(sentence.id_number) + ":" + wordnums_string
            freq_surface.append(source_sent_id)
            info_for_ngram_basestring[current_corpus_name] \
                = (surfaces_dict, total_freq + 1)

################################################################################


def build_candidates(all_entities, print_cand_freq=False, print_source=False, ctxinfo=None):
    """ Builds Candidates list `cands` from an OrderedDict `all_entities` 
    """
    candFact = CandidateFactory()

    candidates = []
    for ngram_basestring_general, info in all_entities.items():
        cand = candFact.make()
        cand.from_string(ctxinfo, ngram_basestring_general)

        for corpus_name, (surface_dict, total_freq) in info.items():
            if print_cand_freq:
                freq = Frequency(corpus_name, total_freq)
                cand.add_frequency(freq)

            # Process all surface occurences from candidate while creating new Ngrams
            for occur_string in list(surface_dict.keys()):

                # Loads occurences from candidate
                sources = surface_dict[occur_string]
                freq = Frequency(corpus_name, len(sources))

                # Creates Ngram object and assigns frequency to it
                occur_form = Ngram(None, None)
                occur_form.from_string(ctxinfo, occur_string)
                occur_form.add_frequency(freq)

                if print_source:
                    occur_form.add_sources(sources)
                cand.add_occur(occur_form)

        candidates.append(cand)

    return candidates
