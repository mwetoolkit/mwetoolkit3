#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

import pytest
import operator

from ... import util
from ..feature import FeatureSet
from ..word import Word, SEPARATOR, WORD_SEPARATOR
from ..frequency import Frequency
from ..ngram import Ngram

def WordList1():
    return [Word({"surface":"Charles","lemma":"Charles","pos":"noun","syn":"nsubj:1"}), Word({"surface":"walks","lemma":"walk","pos":"verb","syn":"root:0"})]

def WordList2():
    return [Word({"surface":"three","lemma":"three","pos":"number"}), Word({"surface":"four","lemma":"walk","pos":"verb","syn":"nmod:1"})]

def test___init__():
    ng1 = Ngram([123,456,789], None, None)


def test_append():
    #normal case
    ng1 = Ngram(WordList1(), None, None)
    ng1.append(Word({"surface":"fast","lemma":"fast","pos":"adj","syn":":adj"}))
    assert len(ng1.word_list[2]._props) == 4
    assert ng1.word_list[2]._props["lemma"] == "fast"
    assert len(ng1.word_list) == 3

def test_append_empty_ngram():    
    #empty ngram case
    ng2 = Ngram([], None, [1234])
    assert len(ng2.word_list) == 0
    ng2.append(Word({"surface":"yolo","lemma":"yolo"}))
    assert len(ng2.word_list) == 1
    assert len(ng2.word_list[0]._props) == 2

def test__iter__1( ):
    #test on 2 differents words but same props size
    ng1 = Ngram(WordList1(), None, None)
    for element in ng1.__iter__():
        assert isinstance(element, Word) == True
        assert element._props["surface"] != None
        assert len(element._props) == 4

def test__iter__2( ):
    #test on 2 different words
    for element in Ngram(WordList2(), None, None):
        if element.get_prop("surface") == "three":
            assert len(element._props) == 3
            assert element._props["lemma"] == "three"
        else:
            assert len(element._props) == 4
            assert element._props["syn"] == "nmod:1"

def test___len__():
    #normal use
    assert len(Ngram(WordList1(), None, None)) == 2
    assert len(Ngram([], None, [1234])) == 0
    word_set1 = [Word({}), Word({"lemma":"captain"}),Word({"surface":"falcon"}), Word({"lemma":"punch"})]
    ng1 = Ngram(word_set1, None, [12])
    assert len(ng1) == 4

def test___len__add():
    #add one word
    word_set1 = [Word({}), Word({"lemma":"captain"}),Word({"surface":"falcon"}), Word({"lemma":"punch"})]
    ng1 = Ngram(word_set1, None, [12])
    ng1.word_list.append(Word({"surface":"!"}))
    assert len(ng1) == 5

def test___len__remove():
    #remove one word
    word_set1 = [Word({}), Word({"lemma":"captain"}),Word({"surface":"falcon"}), Word({"lemma":"punch"})]
    ng1 = Ngram(word_set1, None, [12])
    ng1.word_list.pop(2)
    assert len(ng1.word_list) == 3

def test___getitem__1():
    #normal use
    ng1 = Ngram(WordList1(), None, None)
    assert isinstance(ng1[0], Word)
    assert len(ng1[0]._props) == 4
    assert ng1[0]._props["lemma"] == "Charles"

def test___getitem__2():
    #empty Ngram
    ng1 = Ngram(WordList1(), None, None)
    ng2 = Ngram([], None, None)
    with pytest.raises(IndexError):
        ng2[0]
    with pytest.raises(IndexError):
        ng1[5]
    
    ng1.word_list.append(Word({"surface":"fast", "lemma":"fast"}))
    assert ng1[2]._props["surface"] == "fast"


def test___hash1():
    #two same ngrams from different sources
    ng1 = Ngram(WordList1(), None, [12,19])
    ng2 = Ngram(WordList1(), None, [3])
    assert hash(ng1) == hash(ng2)

def test___hash2():
    #two empty ngrams from differents sources
    ng4 = Ngram([], None, [95])
    assert hash(ng4) == hash(Ngram([], None, [3,56,46,2187]))

def test___hash3():
    #same keys
    ng1 = Ngram(WordList1(), None, [12,19])
    ng3 = Ngram(WordList2(), None, None)
    assert hash(ng1) != hash(ng3)

def test___hash4():
    #we add manualy dict1
    ng2 = Ngram(WordList1(), None, [3])
    ng4 = Ngram([], None, [95])
    ng4.word_list.append(Word({"surface":"Charles","lemma":"Charles","pos":"noun","syn":"nsubj:1"}))
    ng4.word_list.append(Word({"surface":"walks","lemma":"walk","pos":"verb","syn":"root:0"}))
    assert hash(ng4) == hash(ng2)

def test__hash5():
    ng2 = Ngram(WordList1(), None, [3])
    ng4 = Ngram([], None, [95])
    ng4.word_list.append(Word({"surface":"Charles","lemma":"Charles","pos":"noun","syn":"nsubj:1"}))
    ng4.word_list.append(Word({"surface":"walks","lemma":"walk","pos":"verb","syn":"root:0"}))
    #we remove one word from ng4
    ng4.word_list.pop(1)
    assert hash(ng4) != hash(ng2)

def test_hash6():
    ng4 = Ngram([], None, [95])
    ng4.word_list.append(Word({"surface":"Charles","lemma":"Charles","pos":"noun","syn":"nsubj:1"}))
    ng4.word_list.append(Word({"surface":"walks","lemma":"walk","pos":"verb","syn":"root:0"}))
    ng4.word_list.pop(1)
    #ng4 is empty
    ng4.word_list.pop(0)
    assert hash(ng4) == hash(Ngram([], None, [8,87]))

def test_foreach_del_prop():

    #normal use
    ng1 = Ngram(WordList2(), None, [3,6])
    ng1.foreach_del_prop("lemma")
    
    assert "lemma" not in ng1.word_list[0]._props
    assert "lemma" not in ng1.word_list[1]._props

def test_foreach_del_prop2():
    ng2 = Ngram([Word({"surface":"lmao"}), Word({"surface":"sword","lemma":"sword"}), Word({"lemma":"sense"})], None, None)
    #we delete surface for every Word in wordlist
    ng2.foreach_del_prop("surface")
    assert ng2.word_list[0]._props == {}
    assert len(ng2.word_list[1]._props) == 1

def test_foreach_del_prop3():
    ng2 = Ngram([Word({"surface":"lmao"}), Word({"surface":"sword","lemma":"sword"}), Word({"lemma":"sense"})], None, None)
    #we delete lemma + surface for every Word in wordlist
    ng2.foreach_del_prop("surface")
    ng2.foreach_del_prop("lemma")    
    for element in ng2.word_list:
        assert element._props == {}
