#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

################################################################################
#
# Copyright 2010-2014 Carlos Ramisch, Vitor De Araujo, Silvio Ricardo Cordeiro,
# Sandra Castellanos
#
# meta.py is part of mwetoolkit
#
# mwetoolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mwetoolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mwetoolkit.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
"""
    This module provides the marker classes.
    These are non-content classes whose instances represent some metadata
    (e.g. a marker is yielded before handling each file).
"""

import collections


################################################################################

class Marker:
    """Base class whose instances represent some metadata
    (e.g. a marker is yielded before handling each file).
    """

class MarkerBeforeFile(Marker, collections.namedtuple('MarkerBeforeFile', 'fileobj ctxinfo')):
    r"""Marker yielded before anything else, for each file."""
    DISPATCH = 'before_file'

class MarkerAfterFile(Marker, collections.namedtuple('MarkerAfterFile', 'fileobj ctxinfo')):
    r"""Marker yielded after anything else, for each file."""
    DISPATCH = 'after_file'

