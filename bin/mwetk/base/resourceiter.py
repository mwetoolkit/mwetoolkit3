#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

################################################################################
#
# Copyright 2010-2019 Carlos Ramisch, Vitor De Araujo, Silvio Ricardo Cordeiro,
# Sandra Castellanos
#
# resourceiter.py is part of mwetoolkit
#
# mwetoolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mwetoolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mwetoolkit.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
"""
This module provides the `Resource` class.

(Note: this module could have been called `resource.py` instead of `resourceiter.py`,
but this would conflict with another python lib. Python does not make any effort
to distinguish local from global imports, and we have already had a similar problem
in the past with `pattern.py`, which we renamed to `patternlib.py`.)
"""


import collections
import itertools

from .. import filetype


class Resource:
    r"""Lazy iterator with internal cache."""
    def __init__(self, sub_iter):
        r"""Create a Resource with given sub-iterator."""
        self.cache = collections.deque()
        self.sub_iter = iter(sub_iter)
        self.already_yielded = False

    def __iter__(self):
        r"""Return self."""
        return self

    def __next__(self):
        r"""Return next element (from cache or from sub_iter)."""
        self.already_yielded = True
        if self.cache:
            return self.cache.popleft()
        return next(self.sub_iter)

    def __getitem__(self, item):
        r"""If `item` is an integer, returns the value: self[i].
        If `item` is a slice, returns a list of values: self[iA:iB].
        Raises IterBoundsError on error.

        Note: This method does not return a Resource.
        Consider using `self.slice(iA, iB)` instead.
        """
        if isinstance(item, int):
            if _isneg(item):
                raise ValueError('Indexes cannot be negative')
            self.cache_at_least(item+1)
            return self.cache[item]

        elif isinstance(item, slice):
            if _isneg(item.start) or _isneg(item.stop):
                raise ValueError('Slice indexes cannot be negative')
            self.cache_at_least(item.stop)
            return list(itertools.islice(self.cache, item.start, item.stop, item.step))

        raise ValueError(
            'Expected integer index or slice, not {:r}'
            .format(type(item).name))

    def cache_all(self):
        r"""Put ALL sub-elements in cache."""
        self.cache_hopefully(float('inf'))

    def cache_hopefully(self, n_elems: int):
        r"""Try to make sure at least `n` elems are cached.
        No error is reported if there are not enough elements.
        """
        try:
            return self.cache_at_least(n_elems)
        except IterBoundsError:
            return self

    def cache_at_least(self, n_elems: int):
        r"""Guarantee at least `n` elems are cached.
        Raises an IterBoundsError on error.
        """
        try:
            while len(self.cache) < n_elems:
                elem = next(self.sub_iter)
                self.cache.append(elem)
            return self
        except StopIteration:
            raise IterBoundsError from None

    def __repr__(self):
        return self._repr_upto(7)

    def _repr_upto(self, n_elems):
        self.cache_hopefully(n_elems+1)
        elems = itertools.islice(self.cache, 0, n_elems)
        if not elems:
            return "Resource({})"
        repr_elems = [repr(x) for x in elems]
        if len(self.cache) > n_elems:
            repr_elems.append("...")
        inner = ",\n".join("  {}: {}".format(i, x) for i, x in enumerate(repr_elems))
        return "Resource({{\n{}\n}})".format(inner)


class IterBoundsError(Exception):
    r"""Raised when trying to access CachedIter at an invalid index."""
    # XXX should this inherit from IndexError? (its usage is kind of weird...)
    pass


def _isneg(value):
    import numbers
    return isinstance(value, numbers.Number) and value < 0
