#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

from .._common import Escaper 


def test_escape_unescape():
    esc1 = Escaper("*[","]", [("*","*[asterix]" ),("+" ,"*[plus]")])
    esc2 = Escaper("^/" ,"/*", [("^", "^/hat/*"), ("~","^/wave/*")])
    assert esc2.escape("IamA~, fake /~/, baited /*") == "IamA^/wave/*, fake /^/wave/*/, baited /*"

    assert esc1.escape("*[asterix]*Haha*[lmao]+*[hihihi]") == "*[asterix][asterix]*[asterix]Haha*[asterix][lmao]*[plus]*[asterix][hihihi]"
    assert esc1.unescape(esc1.escape("*[asterix]*Haha*[lmao]+*[hihihi]")) == "*[asterix]*Haha*[lmao]+*[hihihi]"
