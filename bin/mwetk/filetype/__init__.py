#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

################################################################################
#
# Copyright 2010-2015 Carlos Ramisch, Vitor De Araujo, Silvio Ricardo Cordeiro,
# Sandra Castellanos
#
# filetype.py is part of mwetoolkit
#
# mwetoolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mwetoolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mwetoolkit.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
"""
This module provides classes and methods for filetype detection,
parsing and printing.
"""

from ..base.candidate import Candidate
from ..base.sentence import Sentence
from ..base.word import Word
from ..base.resourceiter import Resource
from .. import util

from . import _common as common


################################################################################

# Leak very common stuff into this namespace
from ._common import autoload, StopParsing, InputHandler, \
        ChainedInputHandler, Directive, InputObj


################################################################################

def load(resource_spec, *, filetype_hint=None, parser=None) -> Resource:
    r"""Return a Resource object for given
    resource_spec (file path, fileobj or InputObj),
    """
    inputobjs = common.make_inputobjs(resource_spec, require_list=False)
    resource = Resource(iter_parse(
        inputobjs, filetype_hint=filetype_hint, parser=parser))
    for inputobj in inputobjs:
        inputobj.resource = resource
    return resource


def parse_gen(input_fileobjs, filetype_hint=None, parser=None):
    """ Generator wrapper for a Entity parser
    """
    input_fileobjs = common.make_inputobjs(input_fileobjs)
    parser = _get_parser(input_fileobjs, parser, filetype_hint)

    yield from iter_parse(input_fileobjs, filetype_hint=None, parser=parser)


def parse(input_fileobjs, handler, filetype_hint=None, parser=None):
    r"""For each input fileobj, detect its file format,
    parse it and call the appropriate handler methods.

    You MUST call this function when parsing a file.
    Don't EVER call `parser.iter_parse` directly, or you will
    suffer HARSH CONSEQUENCES. You have been warned.

    @param input_fileobjs: a list of file objects to be parsed.
    @param handler: an instance of InputHandler.
    @param filetype_hint: either None or a valid filetype_ext string.
    @param parser: either None or an instance of AbstractParser.
    """
    entities_gen = parse_gen(input_fileobjs, filetype_hint, parser)
    with HandlerWrapper(handler, parser) as hw:
        for entity in entities_gen:
            hw.latest_ctxinfo = entity.ctxinfo
            hw.handler.handle(entity, entity.ctxinfo)


def iter_parse(input_fileobjs, *, filetype_hint=None, parser=None):
    r"""For each input fileobj, detect its file format,
    parse it and call the appropriate handler methods.

    You MUST call this function when parsing a file.
    Don't EVER call `parser.iter_parse` directly, or you will
    suffer HARSH CONSEQUENCES. You have been warned.

    @param input_fileobjs: a list of file objects to be parsed.
    @param filetype_hint: either None or a valid filetype_ext string.
    @param parser: either None or an instance of AbstractParser.
    """
    input_fileobjs = common.make_inputobjs(input_fileobjs)
    parser = _get_parser(input_fileobjs, parser, filetype_hint)
    for input_file in input_fileobjs:
        yield from parser.iter_parse(input_file)
        input_file.close()


def _get_parser(inputobjs, parser, filetype_hint):
    r"""Return `parser` itself, or a new parser based
    on `filetype_hint` if `parser is None`.

    @param inputobjs: a list of InputObj's
    @param filetype_hint: either None or a valid filetype_ext string.
    @param parser: either None or an instance of AbstractParser.
    """
    assert not (parser and filetype_hint), \
            "Pass filetype_hint to the ctor of the parser instead"
    assert isinstance(inputobjs, list), inputobjs
    assert all(isinstance(iobjs, InputObj) for iobjs in inputobjs), inputobjs
    assert filetype_hint is None or isinstance(filetype_hint, str), filetype_hint
    if parser:
        assert isinstance(parser, common.AbstractParser), parser
        return parser

    parser_class = inputobjs[0]._parser_class(filetype_hint)
    return parser_class()



class HandlerWrapper:
    r"""Context manager that should be used at top-level
    if ever calling a method on an InputHandler.

    Don't EVER call e.g. `parser.parse` directly without having
    a HandleWrapper somewhere along the way, or you will suffer
    HARSH CONSEQUENCES.  You have been warned.
    """
    def __init__(self, handler, parser):
        self.inner_handler = handler
        self.parser = parser
        self.latest_ctxinfo = None

    def __enter__(self):
        self.handler = FirstInputHandler(self.inner_handler)
        return self

    def __exit__(self, excpt_type, value, excpt_trace_back):
        self.handler.exiting()
        self.assign_ctxinfo(value)

        suppress_exception = True
        if value is None:
            pass  # No exception; just continue
        elif isinstance(value, StopParsing):
            pass  # Exception was raised just to stop parsing, so we stop
        elif isinstance(value, IOError):
            suppress_exception = self.check_errno(value)
        else:
            return False  # re-raise exception

        try:
            ctxinfo = util.SimpleContextInfo("<parsing input files>")
            self.handler.finish(ctxinfo)
        except IOError as e:
            suppress_exception = self.check_errno(e)
        return suppress_exception


    def check_errno(self, exception):
        r"""Suppress errno=EPIPE, because it just means a closed stdout."""
        import errno
        return exception.errno == errno.EPIPE


    def assign_ctxinfo(self, exception):
        """Assign to `exception.latest_ctxinfo`."""
        try:
            ctxinfo = self.parser.latest_ctxinfo
        except AttributeError:
            ctxinfo = self.latest_ctxinfo

        if not hasattr(exception, 'ctxinfo'):
            try:
                exception.ctxinfo = ctxinfo
            except AttributeError:
                pass  # If we cannot assign a ctxinfo, just continue



###########################################################

class DelegatorHandler(InputHandler):
    r"""InputHandler that can delegate every call
    to another InputHandler at a later time.
    """
    def __init__(self):
        self.handlables = []

    def _fallback(self, entity, ctxinfo):
        entity.ctxinfo = ctxinfo
        self.handlables.append(entity)

    def delegate_to(self, another_handler):
        r"""Delegate every `handle` call to `another_handler`."""
        with HandlerWrapper(another_handler, None):
            cur_ctxinfo = None
            for handlable in self.handlables:
                if cur_ctxinfo and cur_ctxinfo.inputobj is not handlable.ctxinfo.inputobj:
                    another_handler.after_file(cur_ctxinfo.inputobj.fileobj, cur_ctxinfo)
                    cur_ctxinfo = None
                if not cur_ctxinfo:
                    cur_ctxinfo = handlable.ctxinfo
                    another_handler.before_file(cur_ctxinfo.inputobj.fileobj, cur_ctxinfo)
                another_handler.handle(handlable, handlable.ctxinfo)
            if cur_ctxinfo:
                another_handler.after_file(cur_ctxinfo.inputobj.fileobj, cur_ctxinfo)


###########################################################


def parse_entities(input_files, filetype_hint=None):
    r"""For each input file, detect its file format and parse it,
    returning a list of all parsed entities.
    
    @param input_files: a list of file objects
    whose contents should be parsed.
    @param filetype_hint: either None or a valid
    filetype_ext string.
    """
    handler = EntityCollectorHandler()
    parse(input_files, handler, filetype_hint)
    return handler.entities


class EntityCollectorHandler(InputHandler):
    r"""InputHandler that collects ALL entities together
    in `self.entities`. Will fail with an out-of-memory
    error if used on huge inputs."""
    def __init__(self):
        self.entities = []

    def _fallback_entity(self, entity, ctxinfo):
        entity.ctxinfo = ctxinfo
        self.entities.append(entity)

    def handle_comment(self, comment, ctxinfo):
        pass  # Just ignore them


################################################################################


def printer_class(ctxinfo, filetype_ext):
    r"""Return a subclass of AbstractPrinter for given filetype extension.
    If you want a printer class that automatically handles all categories,
    create an instance of AutomaticPrinterHandler instead.
    """
    try:
        return common.autoload().hint2info[filetype_ext.lower()].get_printer_class(ctxinfo)
    except KeyError:
        ctxinfo.error("Unknown file extension {ext}", ext=filetype_ext)


################################################################################


class FirstInputHandler(ChainedInputHandler):
    r"""First instance of InputHandler in a chain.
    This InputHandler does some general processing before
    passing the arguments over to the actual handlers.
    
    Tasks that are performed here:
    -- print_progress: warning the user about what
    has already been processed.
    -- handle_meta_if_absent: guarantee that `handle_meta`
    has been called when handling entities.
    """
    PROGRESS_EVERY = 100

    def __init__(self, chain):
        self.chain = chain
        self.count = 0
        self._meta_handled = False

    def _fallback_entity(self, entity, ctxinfo):
        self.count += 1
        self.print_progress(ctxinfo)
        self.chain.handle(entity, ctxinfo)
        
    def handle_candidate(self, candidate, ctxinfo):
        self.handle_meta_if_absent(ctxinfo)
        self._fallback_entity(candidate, ctxinfo)
    
    def handle_meta(self, meta, ctxinfo):
        self._meta_handled = True
        self.chain.handle_meta(meta, ctxinfo)

    def handle_meta_if_absent(self, ctxinfo):
        if not self._meta_handled:
            from ..base.meta import Meta
            self.handle_meta(Meta(None,None,None), ctxinfo)

    def print_progress(self, ctxinfo):
        if self.count % self.PROGRESS_EVERY == 0:
            a, b = ctxinfo.inputobj.current_progress()
            if b == 0:
                percent = ""
            else:
                p = round(100 * (a/b), 0)
                if p == 100.0:
                    p = 99.0  # "100%" looks fake...
                percent = " ({:2.0f}%)".format(p)

            if util.verbose_on:
                util.verbose("\r~~> Processing entity number {n}{percent}\x1b[0K"
                        .format(n=self.count, percent=percent), end="",
                        printing_progress_now=True)
                util.just_printed_progress_line = True

    def exiting(self):
        r"""(Finish the job of print_progress)."""
        if util.just_printed_progress_line:
            util.verbose("", end="")

################################################################################


class AutomaticPrinterHandler(ChainedInputHandler):
    r"""Utility subclass of ChainedInputHandler that automatically
    creates an appropriate printer by calling `make_printer` with
    information from the first input file.
    """
    def __init__(self, forced_filetype_ext):
        self.forced_filetype_ext = forced_filetype_ext

    def before_file(self, fileobj, ctxinfo):
        if not self.chain:
            self.chain = self.make_printer(ctxinfo, self.forced_filetype_ext)
        self.chain.before_file(fileobj, ctxinfo)



################################################################################

if __name__ == "__main__" :
    import doctest
    doctest.testmod()
