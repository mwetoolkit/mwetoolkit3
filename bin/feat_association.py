#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

################################################################################
#
# Copyright 2010-2015 Carlos Ramisch, Vitor De Araujo, Silvio Ricardo Cordeiro,
# Sandra Castellanos
#
# feat_association.py is part of mwetoolkit
#
# mwetoolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mwetoolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mwetoolkit.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
"""
    This script adds five new features for each candidate in the list and for
    each corpus with a known size. These features correspond to Association
    Measures (AMs) based on the frequency of the ngram compared to the frequency
    of the individual words. The AMs are:
        mle: Maximum Likelihood Estimator
        pmi: Pointwise Mutual Information
        t: Student's t test score
        dice: Dice's coeficient
        ll: Log-likelihood (bigrams only)
    Each AM feature is suffixed by the name of the corpus from which it was
    calculated.
    
    For more information, call the script with no parameter and read the
    usage instructions.
"""

# from bin.mwetk.base import candidate
from mwetk.base.meta_feat import MetaFeat
from mwetk import util
from mwetk import filetype
from mwetk.feat_association import *



################################################################################     
# GLOBALS     
     
usage_string = """\
Usage: {progname} OPTIONS <candidates>
Read frequency-annotated MWE lexicon.
Output MWE lexicon with association scores.

The <candidates> input file must be in one of the filetype
formats accepted by the `--from` switch.


OPTIONS may be:

--from <input-filetype-ext>
    Force reading from given filetype extension.
    (By default, file type is automatically detected):
    {descriptions.input[candidates]}

--to <output-filetype-ext>
    Write output in given filetype extension.
    (By default, keeps original input format):
    {descriptions.output[candidates]}

-m <meas> OR --measures <meas>
    The name of the measures that will be calculated. If this option is not
    defined, the script calculates all available measures. Measure names should
    be separated by colon ":" and should be in the list of supported measures
    below:

    mle -- Maximum Likelihood Estimator
    pmi -- Pointwise Mutual Information
    t -- Student's t test score
    dice -- Dice's coeficient
    ll -- Log-likelihood (bigrams only)

-o <name> OR --original <name>
    The name of the frequency source from which the candidates were extracted
    originally. This is only necessary if you are using backoff to combine the
    counts. In this case, you MUST define the original count source and it must
    be a valid name described through a <corpussize> element in the meta header.

-u OR --unnorm-mle
    Does not normalize Maximum Likelihood Estimator. This means that instead of
    the raw frequency divided by the size of the corpus, MLE will correspond to
    the simple raw frequency of the n-gram. Both, normalized and unnormalized
    MLE are rank-equivalent.

{common_options}
"""
input_filetype_ext = None
output_filetype_ext = None

supported_measures = [ "mle", "t", "pmi", "dice", "ll" ]
measures = supported_measures
not_normalize_mle=False

     
################################################################################     

class FeatureAdderPrinter(filetype.ChainedInputHandler):

    def __init__(self):
        self.corpussize_dict = {}
        # TODO: Parametrable combine function
        self.heuristic_combine = lambda l : sum( l ) / len( l ) # Arithmetic mean
        self.warn_ll_bigram_only = True


    def before_file(self, bf, ctxinfo):
        if not self.chain:
            self.chain = self.make_printer(ctxinfo, output_filetype_ext)
        self.chain.before_file(bf, ctxinfo)

    def handle_meta(self, meta, ctxinfo):
        """Adds new meta-features corresponding to the AM features that we add to
        each candidate. The meta-features define the type of the features, which
        is a real number for each of the 4 AMs in each corpus.
        
        @param meta The `Meta` header that is being read from the XML file.       
        """
        global corpussize_dict
        global measures

        for corpus_size in meta.corpus_sizes :
            # Init copurssize_dict with sizes of different corpora
            self.corpussize_dict[ corpus_size.name ] = float(corpus_size.value) 
        for corpus_size in meta.corpus_sizes :
            for meas in measures :
                meta.add_meta_feat(MetaFeat( meas+ "_" +corpus_size.name, "real" ))
        self.chain.handle_meta(meta, ctxinfo)


    def handle_candidate(self, candidate, ctxinfo):
        """For each candidate and for each `CorpusSize` read from the `Meta` 
        header, generates four features that correspond to the Association
        Measures described above.
        
        @param candidate The `Candidate` that is being read from the XML file.    
        """
        global corpussize_dict, main_freq, measures, heuristic_combine, not_normalize_mle, warn_ll_bigram_only
        main_freq = None

        compute_metrics(
            ctxinfo, candidate, self.corpussize_dict, 
            main_freq, measures, self.heuristic_combine,
            not_normalize_mle, self.warn_ll_bigram_only)

        self.chain.handle_candidate(candidate, ctxinfo)


################################################################################

def interpret_measures( measures_string ) :
    """
        Parses the names of the AMs from the command line. It verifies that the
        names of the AMs are valid names of available measures that can be
        calculated by the script.
        
        @param measures_string A string containing the names of the AMs the user
        wants to calculate separated by ":" colon.
        
        @return A list os strings containing the names of the AMs we need to 
        calculate.
    """

    # Scripy specific, only validates metrics passed by command line argument
    global supported_measures
    measures_list = measures_string.split( ":" )
    result = []
    for meas_name in measures_list :
        if meas_name in supported_measures :
            result.append( meas_name )
        else :
            raise ValueError("ERROR: measure is not supported: "+str(meas_name))
    return result

################################################################################

def treat_options( opts, arg, n_arg, usage_string ) :
    """
        Callback function that handles the command line options of this script.

        @param opts The options parsed by getopts. Ignored.

        @param arg The argument list parsed by getopts.

        @param n_arg The number of arguments expected for this script.
    """
    global measures
    global supported_measures
    global main_freq
    global not_normalize_mle
    global input_filetype_ext
    global output_filetype_ext
    
    ctxinfo = util.CmdlineContextInfo(opts)
    util.treat_options_simplest(opts, arg, n_arg, usage_string)

    for o, a in ctxinfo.iter(opts):
        if o in ( "-m", "--measures" ) :
            measures = ctxinfo.parse_list(a, ":", supported_measures)
        elif o in ( "-o", "--original" ) :
            main_freq = a
        elif o in ( "-u", "--unnorm-mle" ) :
            not_normalize_mle = True
        elif o == "--from":
            input_filetype_ext = a
        elif o == "--to":
            output_filetype_ext = a
        else:
            raise Exception("Bad arg: " + o)


################################################################################
# MAIN SCRIPT

longopts = ["from=", "to=", "measures=", "original=", "unnorm-mle"]
args = util.read_options( "m:o:u", longopts, treat_options, -1, usage_string )

printer = FeatureAdderPrinter()
filetype.parse(args, printer, input_filetype_ext)
